import requests

# r= requests.get('http://localhost:8000/ratePro/profs/')
# r = requests.get('http://localhost:8000/ratePro/profs/', auth=('asd', 'asd'))
# r = requests.post('http://localhost:8000/ratePro/modules/', data = {'name':'value', 'code':'test'})
# r = requests.post('http://localhost:8000/ratePro/users/', data = {'username':'realone', 'password':'real1973'})

logged_in = False
url = 'http://localhost:8000/ratePro/'
while True:
    s = requests.Session()
    command = input("\n------------------------------------------------\nPrevious output might contain useful information.\n\nPlease enter the command. Enter 'quit' to quit.\n\nAvailable commands:\nregister\nlogin url\nlogout\nlist\nview\naverage professor_id module_code\nrate professor_id module_code year semester rating\n\n>")
    if command == 'register':
        username = input("\nEnter your username\n")
        email = input("\nEnter your email\n")
        password = input("\nEnter your password\n")
        r = s.post(url+'users/', data = {'username':username,'email':email, 'password':password})
        if r.text.startswith('{"url"'):
            print ('User successfully created')
        else:
            print (r.text)

    elif command.startswith('login '):
        url = command[6:]
        username = input("\nEnter your username\n")
        password = input("\nEnter your password\n")
        r = s.get(url, auth=(username, password))
        if r.text.startswith('{"modules"'):
            print ('Logged in')
            logged_in = True
        else:
            print (r.text)

    elif command == 'logout':
        logged_in = False
        s.close()
        print ("\nDisconnected from current session\n")

    elif command == 'list':
        r = s.get(url+'teachings/')
        data = r.json()
        print ('{:^6}'.format('Code') + '{:^30}'.format('Name') + '{:^12}'.format('Year') + '{:^12}'.format('Semester') + '{:^20}'.format('Taught by'))
        for record in data:
            print ('{:^6}'.format(record["modules"][1]) +'{:^30}'.format(record["modules"][0]) + '{:^12}'.format(record["year"]) + '{:^12}'.format(record["semester"]),end='')
            for prof in record["professors"]:
                print ('{:>3},Professor {:<5}   '.format(prof[1],prof[0]),end='')
            print("\n")
        print("\n")

    elif command == 'view':
        r = s.get(url+'view/')
        data = r.json()
        for record in data:
            print("The rating of Professor {}({}) is {}".format(record["professors"][0],record["professors"][1],"*"*record["ave_rate"]))

    elif command.startswith('average '):
        arguments = command[8:].split()
        if (len(arguments) ==2):
            r = s.get(url+'average?prof={}&module={}'.format(arguments[0],arguments[1]))
            if "Not found" in r.text:
                print ('No record found')
            else:
                data = r.json()
                record = data[0]
                print ("The rating of Professor {}({}) in module {} ({}) is {}".format(record["professors"][0],record["professors"][1],record["teachings"][0],record["teachings"][1],"*"*record["ave_rate"]))
        else:
            print ("\nPlease provide professor ID and module code")

    elif command.startswith('rate '):
        if logged_in:
            arguments = command[5:].split()
            if (len(arguments) ==5):
                r = s.get(url+'ratings?prof={}&module={}&year={}&semester={}&rating={}'.format(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4]), auth=(username, password))
                if "error" in r.text:
                    print ('\nNo record found')
                else:
                    print ('\nSuccessfully rated as:{}'.format(username))
            else:
                print ("\nPlease provide professor ID, module code, year, semester and rating")
        else:
            print ("you must be logged in")
    elif command == 'quit':
        break

    else:
        print ("command invalid")



# s = requests.Session()
# s.auth = ('test', 'test')
# # s.auth = ('admin', 'admin')
# # r = s.post('http://localhost:8000/ratePro/users/', data = {'username':'test','email':'test@test.com', 'password':'test'})
#
# # r = s.post('http://localhost:8000/ratePro/ratings/', data = {'professors':'qwe','teachings':'1', 'rate':'4', 'rater':'realtwo'})
# # r = s.get('http://localhost:8000/ratePro/ratings/', data = {'professors':'qwe','teachings':'1', 'rate':'4', 'rater':'realtwo'})
#
# # r = s.get('http://localhost:8000/ratePro/ratings?prof=12&module=78&year=2018&semester=2&rating=1')
#
# r = s.get('http://localhost:8000/ratePro/average?prof=12&module=78')
# data = r.json()
# record = data[0]
# print ("The rating of Professor {}({}) in module {} ({}) is {}".format(record["professors"][0],record["professors"][1],record["teachings"][0],record["teachings"][1],"*"*record["ave_rate"]))

# #view
# r = s.get('http://localhost:8000/ratePro/view')
# data = r.json()
# for record in data:
#     print("The rating of Professor {}({}) is {}".format(record["professors"][0],record["professors"][1],"*"*record["ave_rate"]))


# #List
# r = s.get('http://localhost:8000/ratePro/teachings/')
# data = r.json()
# print ('{:^6}'.format('Code') + '{:^20}'.format('Name') + '{:^12}'.format('Year') + '{:^12}'.format('Semester') + '{:^20}'.format('Taught by'))
# for record in data:
#     print ('{:^6}'.format(record["modules"][1]) +'{:^20}'.format(record["modules"][0]) + '{:^12}'.format(record["year"]) + '{:^12}'.format(record["semester"]),end='')
#     for prof in record["professors"]:
#         print ('{:^10}'.format(prof[0]),end='')
# print("")





# print(data[0]["professors"][1][1])
# print(data)

# + '{:^20}'.format(record["modules"][0]) + '{:^6}'.format(record["year"]) + '{:^12}'.format(record["semester"]) + '{:^15}'.format(record["professors"][0])
# # importing the requests library
# import requests
#
# # api-endpoint
# URL = "http://maps.googleapis.com/maps/api/geocode/json"
#
# # location given here
# location = "delhi technological university"
#
# # defining a params dict for the parameters to be sent to the API
# PARAMS = {'address':location}
#
# # sending get request and saving the response as response object
# r = requests.get(url = URL, params = PARAMS)
#
# # extracting data in json format
# data = r.json()
#
#
# # extracting latitude, longitude and formatted address
# # of the first matching location
# latitude = data['results'][0]['geometry']['location']['lat']
# longitude = data['results'][0]['geometry']['location']['lng']
# formatted_address = data['results'][0]['formatted_address']
#
# # printing the output
# print("Latitude:%s\nLongitude:%s\nFormatted Address:%s"
# 	%(latitude, longitude,formatted_address))
