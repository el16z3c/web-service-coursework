from django.urls import include,path
from rest_framework import routers
from . import views
from .views import ViewViewSet, AverageViewSet, UserViewSet, ModuleViewSet, TeachingViewSet, RatingViewSet
from django.contrib.auth.models import User

router = routers.DefaultRouter()
router.register(r'modules', ModuleViewSet)
router.register(r'average', AverageViewSet)
router.register(r'view', ViewViewSet)
# router.register(r'modules/(?P<modulename>\d+)', ModuleViewSet)
router.register(r'teachings', TeachingViewSet)
router.register(r'ratings', RatingViewSet)
router.register(r'users', UserViewSet)


urlpatterns = [
    # path('', views.index, name='index'),
    path('', include(router.urls)),
    # path('profs/', ListProfView.as_view(), name="professors-all"),
    # path(r'^profs/(?P<profname>\d+)/$', ListProfView.as_view()),
    path('accounts/', include('django.contrib.auth.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

# urlpatterns = [
#     # path('', views.index, name='index'),
#     path('', include(router.urls)),
#     path('profs/', ListProfView.as_view(), name="professors-all"),
#     path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
# ]
