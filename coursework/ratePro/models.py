from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User

# Create your models here.
class Professor(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('Professor Name', max_length=120)
    code = models.CharField('Professor ID', max_length=120)
    def __str__(self):
        return self.name

class Module(models.Model):
    name = models.CharField('Module Name', max_length=120)
    code = models.CharField('Module Code', max_length=120)
    def __str__(self):
        return self.name

class Teaching(models.Model):
    id = models.AutoField(primary_key=True)
    professors = models.ManyToManyField(Professor)
    modules = models.ForeignKey(Module, on_delete=models.CASCADE)
    year = models.IntegerField('Year Taught', blank=True)
    semester = models.IntegerField('Semester', blank=True, validators=[MinValueValidator(1),MaxValueValidator(2)])
    def __str__(self):
        return "Instance {}".format(self.id)


class Rating(models.Model):
    id = models.AutoField(primary_key=True)
    professors = models.ForeignKey(Professor, on_delete=models.CASCADE)
    teachings = models.ForeignKey(Teaching, on_delete=models.CASCADE)
    rate = models.IntegerField('rate',validators=[MinValueValidator(1),MaxValueValidator(5)])
    ave_rate = models.IntegerField('average rate',default='0')
    rater = models.CharField('Rater Name', max_length=120)
    def __str__(self):
        return "{} rating of Prof. {} in Module {} Instance {} by {}".format(self.rate,self.professors.name,self.teachings.modules.name,self.teachings.id,self.rater)
