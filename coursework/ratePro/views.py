from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, generics
from .serializers import ViewSerializer, AverageSerializer, UserSerializer,ModuleSerializer, ProfSerializer, TeachingSerializer, RatingSerializer
from django.contrib.auth.models import User
from .models import Module, Professor, Teaching, Rating
from django.contrib import auth
from django.db.models import Avg
from decimal import *

# Create your views here.
def index(request):
     return HttpResponse("Rate the professors.")

# def logout_view(request):
#     auth.logout(request)
#     # Redirect to a success page.
#     return HttpResponseRedirect("/account/loggedout/")
# @csrf_exempt
# def HandleRegisterRequest(request):
#     if request.method=='POST':
#         return HttpResponse('not yet implemented')

class ModuleViewSet(viewsets.ModelViewSet):
    queryset = Module.objects.all().order_by('name')
    serializer_class = ModuleSerializer

    def get_queryset(self):
        queryset = Module.objects.all()
        code = self.request.query_params.get('code',None)
        print (code)
        if code is not None:
            queryset = queryset.filter(code=code)
        return queryset


class TeachingViewSet(viewsets.ModelViewSet):
    queryset = Teaching.objects.all().order_by('id')
    serializer_class = TeachingSerializer

class RatingViewSet(viewsets.ModelViewSet):
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer

    def get_queryset(self):
        queryset = Rating.objects.all()
        prof = self.request.query_params.get('prof',None)
        module = self.request.query_params.get('module',None)
        year = self.request.query_params.get('year',None)
        semester = self.request.query_params.get('semester',None)
        rating = self.request.query_params.get('rating',None)
        r = Rating.objects.filter(professors__code=prof,teachings__modules__code=module,teachings__year=year,teachings__semester=semester,rater=self.request.user.username)
        # r = Rating.objects.get_or_create(professors=Professor(code=prof),teachings__modules__code=module,teachings__year=year,teachings__semester=semester,rater=self.request.user.username)
        if not r:
            tmp_prof = Professor.objects.filter(code=prof)[0]
            tmp_teaching = Teaching.objects.filter(modules__code=module,year=year,semester=semester)[0]
            # r = Rating.objects.create(professors=tmp_prof,teachings=tmp_teaching,rate=rating,rater=self.request.user.username)
            Rating.objects.create(professors=tmp_prof,teachings=tmp_teaching,rate=rating,rater=self.request.user.username)
            # print ("test1")
        # r.update(rate=rating,rater=self.request.user.username)=
        # print (self.request.user.username)
        # print ("test2")
        r.update(rate=rating)
        return r

class ViewViewSet(viewsets.ModelViewSet):
    queryset = Rating.objects.all()
    serializer_class =ViewSerializer

    def get_queryset(self):
        finalset = []
        profset = Professor.objects.all()
        for profs in profset:
            queryset = Rating.objects.all()
            queryset = queryset.filter(professors__name=profs.name)
            total = 0
            num = 0
            for record in queryset:
                total += record.rate
                num += 1
            average = total/num
            average = Decimal(average).quantize(Decimal('1.'), rounding=ROUND_HALF_UP)
            for record in queryset:
                record.ave_rate = average
            # if mode is not None:
            print (queryset[0].ave_rate)
            finalset.append(queryset[0])
        return finalset

class AverageViewSet(viewsets.ModelViewSet):
    queryset = Rating.objects.all()
    serializer_class =AverageSerializer

    def get_queryset(self):
        queryset = Rating.objects.all()
        prof = self.request.query_params.get('prof',None)
        module = self.request.query_params.get('module',None)
        mode = self.request.query_params.get('mode',None)
        if module is not None:
            queryset = queryset.filter(professors__code=prof,teachings__modules__code=module)
            if not queryset:
                raise Http404("No record matches the given query.")
            total = 0
            num = 0
            for record in queryset:
                total += record.rate
                num += 1
            average = total/num
            average = Decimal(average).quantize(Decimal('1.'), rounding=ROUND_HALF_UP)
            for record in queryset:
                record.ave_rate = average
        # if mode is not None:
        queryset = queryset[:1]
        return queryset

    # def get_queryset(self):
    #     queryset = Professor.objects.all()
    #     name = self.request.query_params.get('name',None)
    #     print (name)
    #     if name is not None:
    #         queryset = queryset.filter(id='1')
    #         return queryset



class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
