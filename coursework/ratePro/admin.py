from django.contrib import admin
from .models import Module,Professor,Teaching,Rating
# Register your models here.

class teachingAdmin(admin.ModelAdmin):
    filter_horizontal = ('professors',)


admin.site.register(Module)
admin.site.register(Professor)
admin.site.register(Rating)
admin.site.register(Teaching,teachingAdmin)
