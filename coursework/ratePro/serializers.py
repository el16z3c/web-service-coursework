# serialisers.py
from rest_framework import serializers

from .models import Module, Professor, Teaching, Rating
from django.contrib.auth.models import User

class ModuleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Module
        fields = ('name', 'code')

class ProfSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Professor
        fields = ('name', 'code')

class ProfListSerializer(serializers.RelatedField):
     def to_representation(self, value):
         return value.name, value.code

     class Meta:
        model = Professor

class ModuleListSerializer(serializers.RelatedField):
     def to_representation(self, value):
         return value.name, value.code

     class Meta:
        model = Module

class TeachingListSerializer(serializers.RelatedField):
     def to_representation(self, value):
         return value.modules.name, value.modules.code

     class Meta:
        model = Teaching

class TeachingSerializer(serializers.HyperlinkedModelSerializer):
    professors = ProfListSerializer(read_only=True,many=True)
    modules = ModuleListSerializer(read_only=True,many=False)
    class Meta:
        model = Teaching
        fields = ('id', 'professors','modules', 'year', 'semester')

class RatingSerializer(serializers.ModelSerializer):
    professors = serializers.SlugRelatedField(slug_field='name',queryset=Professor.objects.all())
    # professors = ModuleListSerializer(many=False,read_only='True')
    teachings = serializers.SlugRelatedField(slug_field='id',queryset=Teaching.objects.all())
    class Meta:
        model = Rating
        fields = ('professors','teachings', 'rate','ave_rate', 'rater')

    def create(self, validated_data):
        rating = Rating(professors=validated_data['professors'], teachings=validated_data['teachings'], rate=validated_data['rate'], rater=validated_data['rater'])
        rating.save()
        return rating

class ViewSerializer(serializers.HyperlinkedModelSerializer):

    professors = serializers.SlugRelatedField(slug_field='name',queryset=Professor.objects.all())
    professors = ProfListSerializer(read_only=True)
    # teachings = serializers.SlugRelatedField(slug_field='id',queryset=Teaching.objects.all())
    # modules = serializers.SlugRelatedField(slug_field='name',queryset=Module.objects.all())
    # modules = ModuleListSerializer(read_only=True)
    # professors = ModuleListSerializer(many=False,read_only='True')
    class Meta:
        model = Rating
        fields = ('professors','ave_rate')

class AverageSerializer(serializers.HyperlinkedModelSerializer):

    professors = serializers.SlugRelatedField(slug_field='name',queryset=Professor.objects.all())
    professors = ProfListSerializer(read_only=True)
    teachings = TeachingListSerializer(read_only=True)
    # teachings = serializers.SlugRelatedField(slug_field='id',queryset=Teaching.objects.all())
    # modules = serializers.SlugRelatedField(slug_field='name',queryset=Module.objects.all())
    # modules = ModuleListSerializer(read_only=True)
    # professors = ModuleListSerializer(many=False,read_only='True')
    class Meta:
        model = Rating
        fields = ('professors','teachings','ave_rate')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'password','email')

    def create(self, validated_data):
        user = User(email=validated_data['email'], username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        return user
